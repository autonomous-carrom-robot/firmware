void timer_init(void) {

  cli(); //stop interrupts

  // linear motion timer

  TCCR4A = 0;// set entire TCCR2A register to 0
  TCCR4B = 0;// same for TCCR2B
  TCNT4  = 0;//initialize counter value to 0

  //OCR4A = 499;// = (16*10^6) / (500*64) - 1 (must be <256) // set compare match register for 0.5khz increments (2000us)
  //*default max speed*//
  OCR4A = 249;// = (16*10^6) / (1000*64) - 1 (must be <256) // set compare match register for 1khz increments (1000us)
  //OCR4A = 124;// = (16*10^6) / (2000*64) - 1 (must be <256) // set compare match register for 2khz increments (500us)

  TCCR4B |= (1 << WGM42);// turn on CTC mode
  TCCR4B |= (1 << CS41) | (1 << CS50); //Set CS21 bit for 64 prescaler
  //TIMSK4 |= (1 << OCIE4A);// enable timer compare interrupt
  TIMSK4 = 0x00; //disable interrupting

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//

  // rotational motion timer

  TCCR5A = 0;// set entire TCCR2A register to 0
  TCCR5B = 0;// same for TCCR2B
  TCNT5  = 0;//initialize counter value to 0

  //OCR5A = 1999;// 
  OCR5A = 999;// = (16*10^6) / (250*64) - 1 (must be <256) // set compare match register for 0.25khz increments (4000us)
  //**default**//OCR5A = 499;// = (16*10^6) / (500*64) - 1 (must be <256) // set compare match register for 0.25khz increments (2000us)
  //too fast//OCR5A = 249;// = (16*10^6) / (1000*64) - 1 (must be <256) // set compare match register for 1khz increments (1000us)
  //OCR5A = 124;// = (16*10^6) / (2000*64) - 1 (must be <256) // set compare match register for 2khz increments (500us)

  TCCR5B |= (1 << WGM52);// turn on CTC mode
  TCCR5B |= (1 << CS51) | (1 << CS50); //Set CS21 bit for 64 prescaler
  //TIMSK5 |= (1 << OCIE5A);// enable timer compare interrupt
  TIMSK5 = 0x00; //disable interrupting

  sei(); // start interrupts
}

ISR(TIMER4_COMPA_vect) { // linear motion timer
  if (en_linear_motion) {    
    //linear_trajectory_cb();
    linear_callback();
  }
  else if (is_linear_calibrating){
    linear_calibrate_callback();
  }
}

ISR(TIMER5_COMPA_vect) { // rotational motion timer
  if (en_rotational_motion) {
    //rotational_trajectory_cb();
    rotational_callback();
  }
}
