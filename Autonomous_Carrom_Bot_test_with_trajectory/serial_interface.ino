void serial_init(uint32_t baudrate) {
  Serial.begin(baudrate);
  Serial.println("Hi, This is Autonomous Carrom Bot. Let's Play\n");

  Serial.print("Linear Initial Position =");
  Serial.println(linear_current_pos);
  Serial.print("Rotational Initail Position =");
  Serial.println(rotational_current_pos);
  Serial.println(" ");

  Serial.print("Lifter Initial Position =");
  Serial.println(lifter_servo_current_pos);
  Serial.print("Holder Initail Position =");
  Serial.println(holder_servo_current_pos);
  Serial.print("Rotator Initial Position =");
  Serial.println(rotator_servo_current_pos);
  Serial.print("Grabber Initail Position =");
  Serial.println(grabber_servo_current_pos);
  Serial.println(" ");
}

void serial_processor() {
  char command = Serial.read();
  //Serial.println(command);

  if (command == 'l') { // linear motion destination receive
    linear_destination = Serial.parseFloat();
    Serial.print("Setting linear destination to = ");
    Serial.println(linear_destination);
    receive_complete++;
    //linear_runner(linear_destination);
  }
  else if (command == 'r') { // rotatioanl motion destination receive
    rotational_destination = Serial.parseFloat();
    Serial.print("Setting rotational destination to = ");
    Serial.println(rotational_destination);
    if (rotational_destination > 0) {
      rotational_destination-=3;
      Serial.println("HHHHH!!!!!!!!!!!!!!!!!!");
    }
    receive_complete++;
    //rotational_runner(rotational_destination);
  }
  else if (command == 'h') { // shooter height receive
    shooter_destination = Serial.parseInt();
    Serial.print("Setting shooter height to = ");
    Serial.println(shooter_destination);
    receive_complete++;
  }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

  else if (command == 'a') { // linear motion reset destination
    linear_current_pos = Serial.parseFloat();
    EEPROM.put(linear_ee_addr, linear_current_pos);
    Serial.print("Setting Linear Current Position to = ");
    Serial.println(linear_current_pos);
  }
  else if (command == 'b') { // rotational motion reset destination
    rotational_current_pos = Serial.parseFloat();
    EEPROM.put(rotational_ee_addr, rotational_current_pos);
    Serial.print("Setting Rotational Current Position to = ");
    Serial.println(rotational_current_pos);
  }
  else if (command == 'c') { // lifting servo reset position
    lifter_servo_current_pos = Serial.parseInt();
    EEPROM.put(lifter_servo_ee_addr, lifter_servo_current_pos);
    Serial.print("Setting Lifter servo Current Position to = ");
    Serial.println(lifter_servo_current_pos);
  }
  else if (command == 'd') { // holding servo reset position
    holder_servo_current_pos = Serial.parseInt();
    EEPROM.put(holder_servo_ee_addr, holder_servo_current_pos);
    Serial.print("Setting holder servo Current Position to = ");
    Serial.println(holder_servo_current_pos);
  }
  else if (command == 'e') { // rotator servo reset position
    rotator_servo_current_pos = Serial.parseInt();
    EEPROM.put(rotator_servo_ee_addr, rotator_servo_current_pos);
    Serial.print("Setting rotator servo Current Position to = ");
    Serial.println(rotator_servo_current_pos);
  }
  else if (command == 'f') { // grabber servo reset position
    grabber_servo_current_pos = Serial.parseInt();
    EEPROM.put(grabber_servo_ee_addr, grabber_servo_current_pos);
    Serial.print("Setting grabber servo Current Position to = ");
    Serial.println(grabber_servo_current_pos);
  }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  else if (command == 's') { // linear motion direct run
    linear_destination = Serial.parseFloat();
    Serial.print("Setting linear direct run destination to = ");
    Serial.println(linear_destination);
    linear_runner(linear_destination);
  }
  else if (command == 't') { // rotatioanl motion direct run
    rotational_destination = Serial.parseFloat();
    Serial.print("Setting rotational direct run destination to = ");
    Serial.println(rotational_destination);
    rotational_runner(rotational_destination);
  }
  else if (command == 'u') { // lifting servo direct run
    uint8_t lifter_destination = Serial.parseInt();
    Serial.print("Setting Lifter servo direct run to = ");
    Serial.println(lifter_destination);
    lifter_runner(lifter_destination);
  }
  else if (command == 'v') { // holding servo direct run
    uint8_t holder_destination = Serial.parseInt();
    Serial.print("Setting Holder servo direct run to = ");
    Serial.println(holder_destination);
    holder_runner(holder_destination);
  }
  else if (command == 'w') { // rotating servo direct run
    uint8_t rotator_destination = Serial.parseInt();
    Serial.print("Setting Rotator servo direct run to = ");
    Serial.println(rotator_destination);
    rotator_runner(rotator_destination);
  }
  else if (command == 'x') { // grabber servo direct run
    uint8_t grabber_destination = Serial.parseInt();
    Serial.print("Setting Grabber servo direct run to = ");
    Serial.println(grabber_destination);
    grabber_runner(grabber_destination);
  }


  else if (command == 'y') { // calibrate linear position
    linear_calibrate();
    Serial.println("Here");
  }
  else if (command == 'z') { // pendulum pick up
    pendulum_pick_up();
    Serial.print("Pendulum Picked Up");
  }
  else if (command == 'p') {
    grabber_runner(grab_disk); // place disk
    delay(500);
    rotator_runner(rotator_down);
    delay(1500);
    grabber_runner(release_disk);
    delay(500);
    rotator_runner(rotator_up);
    grabber_runner(middle_disk);

  }

  else if (command == 'k') {
    rotational_calibrate();
  }

  else {
    //Serial.println("Invalid Command Received. Please contact Chameera@UoM");
  }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if (receive_complete == 3) {
    //linear_runner(linear_destination);
    //rotational_runner(rotational_destination);
    action = 1;
    receive_complete = 0;
    Serial.flush();
  }
}
