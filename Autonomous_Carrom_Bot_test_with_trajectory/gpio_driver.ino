void gpio_init(void) {
  pinMode(opto_int_pin, INPUT);
  pinMode(linear_begin_int_pin, INPUT);
  pinMode(linear_end_int_pin, INPUT);
  pinMode(manual, INPUT);

  attachInterrupt(digitalPinToInterrupt(linear_begin_int_pin), linear_stop_calibration, RISING);
  attachInterrupt(digitalPinToInterrupt(linear_end_int_pin), linear_stop_moving, RISING);
}

void linear_stop_calibration(void) {
  if (!digitalRead(linear_dir_pin) && !linear_finished) { // moving in reverse direction
    is_linear_calibrating = false; // end callibrating
    TIMSK4 = 0x00; // disable timer compare interrupt for linear motion
    en_linear_motion = false;
    linear_finished = true; // linear motion finished flag
    linear_current_pos = linear_min_dis; // linear min distance occur when begin interrupt hits
    digitalWrite(linear_pulse_pin, LOW);
    EEPROM.put(linear_ee_addr, linear_current_pos);

    Serial.println("Linear Callibration Finished");
    Serial.print("Distance for calibration =");
    Serial.println(linear_steps_to_calibrate / 4.97795);
    linear_steps_to_calibrate = 0;
    Serial.print("Linear Current Position =");
    Serial.println(linear_current_pos);
  }
}

void  linear_stop_moving (void) {
  if (digitalRead(linear_dir_pin) && !linear_finished) { // moving in forward direction
    is_linear_calibrating = false; // end callibrating
    TIMSK4 = 0x00; // disable timer compare interrupt for linear motion
    en_linear_motion = false;
    linear_finished = true; // linear motion finished flag
    linear_current_pos = linear_max_dis; // linear max distance occur when end interrupt hits
    digitalWrite(linear_pulse_pin, LOW);
    EEPROM.put(linear_ee_addr, linear_current_pos);

    Serial.println("Linear Callibration Finished");
    Serial.print("Distance for calibration =");
    Serial.println(linear_steps_to_calibrate / 4.97795);
    linear_steps_to_calibrate = 0;
    Serial.print("Linear Current Position =");
    Serial.println(linear_current_pos);
  }
}

