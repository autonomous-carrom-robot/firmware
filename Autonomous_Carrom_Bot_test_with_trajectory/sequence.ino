void sequence(void) {
  switch (action) {
    case 0:
      break;

    case 1:
      if (is_disk_grabbed) { //disk received has to check here
        linear_calibrate_both();
        action = 2; //chameeraa
      }
      break;

    case 2:
      if (linear_finished) {
        rotational_calibrate();
        action = 3;
        delay(1000);
      }
      break;

    case 3:
      if (linear_finished && rotational_finished) {
        Serial.println(" ");
        Serial.println("Calibration Finished");
        action = 4;
        //delay(1000);
      }
      break;
    case 4:
      linear_runner(linear_destination - 73);  // go to disk placing position
      action = 5;
      //delay(1000);
      break;

    case 5:
      if (linear_finished) { // linear is at disk placing position
        Serial.println(" ");
        Serial.println("Linear current position is at disk placing position");
        Serial.println(" ");
        action = 6;
        delay(1000);
      }
      break;

    case 6:
      rotator_runner(rotator_down); // lower the grabber
      Serial.println("Grabber lowered");
      grabber_runner(release_disk); // release the disk
      is_disk_grabbed = false;
      Serial.println("Disk released");
      //rotator_runner(rotator_up); // lift grabber
      rotator.write(150); //lift grabber fast
      rotator_servo_current_pos = 150;
      EEPROM.put(rotator_servo_ee_addr, rotator_servo_current_pos);
      Serial.println("Grabber lifted");
      grabber_runner(middle_disk);
      Serial.println(" ");
      delay(1000);
      action = 7;
      break;

    case 7:
      linear_runner(linear_destination); //go to shooting position
      lifter_runner(shooter_destination); //lifting the pendulum
      Serial.print("Pendulum Lifted to destination: ");
      Serial.println(shooter_destination);
      Serial.println("Approaching Shooting Position");
      Serial.println(" ");
      //delay(1000);
      action = 8;
      break;

    case 8:
      if (linear_finished) {        
        delay(1000);
        Serial.println(" ");
        Serial.println("Arrived Shooting position");
        rotational_runner(rotational_destination);
        action = 9;
      }
      break;
    case 9:
      if (rotational_finished) {
        delay(1000);
        holder_runner(release_shooter); // shoots
        Serial.println("Shoots....!!!");
        Serial.println(" ");
        pendulum_pick_up();
        delay(2000);

        //Serial.println("p"); //chameera
        Serial.println("");
        action = 10;
        delay(1000);
      }
      break;

    case 10:
      rotational_runner(0);
      linear_runner(222.5);//go to disk receiving position
      action = 11;
      break;

    case 11:
      if (rotational_finished) {
        Serial.println(" ");
        Serial.println("Arrived Disk receiving position and angle");
        Serial.println("JOB DONE...!!!");
        Serial.println(" ");
        action = 0;
      }
      break;
  }
}
