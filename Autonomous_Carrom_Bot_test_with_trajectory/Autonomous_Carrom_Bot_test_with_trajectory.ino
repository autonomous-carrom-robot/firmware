/*
  @ Starting Date : 14.04.2014
  @ User : Chameera@UoM
  @ Bugs : Not Known Bugs
  @ Brief : This is the firmware of Autonomous Carrom Bot
  @ Finalized Date : 17.02.2020
  @ Firmware Version : 1.0
*/
#include <SPI.h>
#include <MagAlpha.h>
#include <EEPROM.h>
#include <Servo.h>

#define linear_begin_int_pin 2 // starting side limit switch 
#define linear_end_int_pin 3 // ending side limit switch

#define manual 9

#define linear_en_pin 27 //FROM NEG SIDE BLUE,GREEN,WHITE,BROWN
#define linear_dir_pin 25
#define linear_pulse_pin 23

#define rotational_en_pin 26 //FROM NEG SIDE RED,BLUE,GREEN,BLACK
#define rotational_dir_pin 24
#define rotational_pulse_pin 22

#define lifter_pin 13
#define holder_pin 12
#define rotator_pin 11
#define grabber_pin 10

#define opto_int_pin A0 // IR opto interrupt pin

#define UART_BAUDRATE       115200        //UART data rate in bits per second (baud)
#define SPI_SCLK_FREQUENCY  10000      //SPI SCLK Clock frequency in Hz 1*e07
#define SPI_CS_PIN          53            //SPI CS pin

uint8_t linear_ee_addr = 1; //EEPROM address of linear current position
uint16_t linear_total_steps = 0; // No of steps to rotate to reach destination
uint16_t linear_reserve_steps = 0; // Steps left to rotate
float linear_current_pos = 0; // Current position
float linear_destination = 0; // Destination
volatile bool en_linear_motion = false; // Enable Calling Interrupt CB
volatile bool linear_finished = true; // Linear motion status

uint16_t linear_max_speed = 250; //time gap between max time period and min time period
uint16_t linear_min_speed = 1;
float linear_accelaration = 0.5; // timer register increments once for two steps
uint16_t linear_min_period = linear_max_speed - 1; // >=249 //this relates to interrupt halfperiod
uint16_t linear_max_period = linear_min_speed + 498; // <=499
uint16_t linear_steps_to_max_speed = 0; //Steps need to acce or deacce
float linear_increment_reg = 0;

uint8_t rotational_ee_addr = 5; //EEPROM address of rotational current position
uint16_t rotational_total_steps = 0; // No of steps to rotate to reach destination
uint16_t rotational_reserve_steps = 0; // Steps left to rotate
float rotational_current_pos = 0; // Current position
float rotational_destination = 0; // Destination
bool en_rotational_motion = false; // Enable Calling Interrupt CB
bool rotational_finished = true; // Rotational motion status

uint16_t rotational_max_speed = 800; //time gap between max time period and min time period
uint16_t rotational_min_speed = 1;
float rotational_accelaration = 2; // timer register increments by 2 for single steps
uint16_t rotational_min_period = rotational_max_speed - 1; // >=799 //this relates to interrupt halfperiod
uint16_t rotational_max_period = rotational_min_speed + 998; // <=999
uint16_t rotational_steps_to_max_speed = 0; //Steps need to acce or deacce
float rotational_increment_reg = 0;

uint8_t shooter_destination = 0;

uint8_t receive_complete = 0; // serial receive counter

uint8_t action = 0; // sequence switch case

bool is_disk_grabbed = false; // disk grabber flag
uint8_t opto_int_threshold = 50;

uint8_t lifter_servo_ee_addr = 9;
uint8_t holder_servo_ee_addr = 11;
uint8_t rotator_servo_ee_addr = 13;
uint8_t grabber_servo_ee_addr = 15;

uint8_t lifter_servo_current_pos = 0;
uint8_t holder_servo_current_pos = 0;
uint8_t rotator_servo_current_pos = 0;
uint8_t grabber_servo_current_pos = 0;

uint8_t lifter_servo_delay = 15;
uint8_t holder_servo_delay = 1;
uint8_t rotator_servo_delay = 10;
uint8_t grabber_servo_delay = 5;

float linear_min_dis = -82.56;
float linear_max_dis = 472.00;
int16_t rotational_min_dis = -180;
int16_t rotational_max_dis = 180;

volatile bool is_linear_calibrating = false;
float linear_steps_to_calibrate = 0;

float encoder_angle_read = 0;
float rotational_zero = 230.7;
//231
//231.84
//232.3;
typedef enum {
  lifter_lower = 180,
  lifter_higher = 5,

  hold_shooter = 110,
  release_shooter = 180,

  rotator_up = 150,
  rotator_middle = 100,
  rotator_down = 44,

  grab_disk = 130,
  middle_disk = 110,
  release_disk = 80,

} SERVO_ACTION;

Servo lifter;
Servo holder;
Servo rotator;
Servo grabber;

MagAlpha magAlpha;


void setup() {
  gpio_init();
  timer_init(); // initialize timers

  linear_stepper_init(); // initialize linear motion
  rotational_stepper_init(); // initialize rotational motion

  servo_init();

  serial_init(115200); //begin serial protocol
  delay(1000);
  magAlpha.begin(SPI_SCLK_FREQUENCY, MA_SPI_MODE_3, SPI_CS_PIN);
}

ISR(TIMER4_COMPA_vect); // interrupt func for timer4, linear4114
ISR(TIMER5_COMPA_vect); // interrupt func for timer5, rotational

void loop() {

  if (digitalRead(manual)) {
    delay(100);
    if (digitalRead(manual)) {
      Serial.println("Disk.......!!!!!!!!");
      grabber_runner(grab_disk); // grab the disk
      is_disk_grabbed = true;
      //delay(1000);
      Serial.println('p');
    }
  }
  serial_processor();

  /*if ((analogRead(opto_int_pin) < opto_int_threshold) && !is_disk_grabbed) { // if disk is on the tray and not locked
    delay(1000);
    grabber_runner(grab_disk); // grab the disk
    delay(1000);
    rotator_runner(rotator_middle);
    delay(750);
    if (analogRead(opto_int_pin) < opto_int_threshold) {
      is_disk_grabbed = true; // disk is grabbed flag
      Serial.println(" ");
      Serial.println("Disk received and grabbed");
      Serial.println(" ");
    }
    else {
      is_disk_grabbed = false; // disk is grabbed flag
      rotator_runner(rotator_up);
      grabber_runner(middle_disk);
      Serial.println("False Alarm...!!!");
    }

    }*/

  //Serial.println(analogRead(opto_int_pin));
  //delay(100);

  sequence();

  //Serial.println(digitalRead(opto_int_pin));
  //delay(100);
  //double angle;
  //Read the angle
  //angle = magAlpha.readAngle();
  //Serial.println(angle, 3);
  //delay(500);
}

/*if (!digitalRead(opto_int_pin) && !is_disk_grabbed) { // if disk is on the tray and not locked
    delay(500);
    grabber_runner(grab_disk); // grab the disk
    delay(1000);
    rotator_runner(rotator_down);
    delay(750);
    grabber_runner(release_disk); // grab the disk
    delay(500);
    rotator_runner(rotator_up);
    is_disk_grabbed = true; // disk is grabbed flag
    Serial.println(" ");
    Serial.println("Disk received and grabbed");
    Serial.println(" ");
  }*/
