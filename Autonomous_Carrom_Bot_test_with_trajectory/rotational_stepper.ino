void rotational_stepper_init(void) {
  pinMode (rotational_en_pin, OUTPUT);
  pinMode (rotational_dir_pin, OUTPUT);
  pinMode (rotational_pulse_pin, OUTPUT);

  digitalWrite(rotational_en_pin, HIGH);

  EEPROM.get(rotational_ee_addr, rotational_current_pos);
}

bool rotational_runner (float destination) {
  if (rotational_finished) {
    if (rotational_min_dis <= destination && destination <= rotational_max_dis) { // bounding inputs
      //digitalWrite(rotational_en_pin, HIGH);
      float distance_to_travel = destination - rotational_current_pos;
      Serial.print("Rotational Distance to Travel = ");
      Serial.println(distance_to_travel);

      digitalWrite(rotational_dir_pin, (distance_to_travel > 0));
      Serial.print("Setting rotational direction to = ");
      Serial.println(distance_to_travel > 0);

      rotational_total_steps = round(abs(distance_to_travel) * 3 / 1.8); // gear ratio 1:3 , full steps
      //rotational_steps = round(abs(distance_to_travel) * 3 / 0.9); // gear ratio 1:3 , half steps
      rotational_reserve_steps = rotational_total_steps;
      Serial.print("Setting rotational steps to = ");
      Serial.println(rotational_reserve_steps);

      if (rotational_reserve_steps > 0) {
        //rotational_trajectory_planning(); // calling the func to start timing
        en_rotational_motion = true;
        rotational_finished = false;
        rotational_current_pos = destination;
        TIMSK5 |= (1 << OCIE5A);  //enable timer compare interrupt
      }
      else {
        TIMSK5 = 0x00;
        Serial.print("rotational Current Position =");
        Serial.println(rotational_current_pos);
      }
    }
    else {
      Serial.println("");
      Serial.println("!!!!Invalid Input for Rotational Destination");
      Serial.println("");
    }
  }

  return true;
}

void rotational_callback(void) {
  if (rotational_reserve_steps == 0) {
    TIMSK5 = 0x00;
    en_rotational_motion = false;
    rotational_finished = true;
    //digitalWrite(rotational_en_pin, LOW);
    Serial.print("Rotational Current Position =");
    Serial.println(rotational_current_pos);
    digitalWrite(rotational_pulse_pin, LOW);
    EEPROM.put(rotational_ee_addr, rotational_current_pos);
  }
  else {
    digitalWrite(rotational_pulse_pin, !digitalRead(rotational_pulse_pin));
    rotational_reserve_steps -= digitalRead(rotational_pulse_pin);
    //Serial.println(rotational_reserve_steps);
  }
}
void rotational_trajectory_planning(void) {
  rotational_steps_to_max_speed = round((rotational_max_period - rotational_min_period) / rotational_accelaration); //100 steps
  Serial.print("rotational_steps_to_max_speed =");
  Serial.println(rotational_steps_to_max_speed);
  OCR5A = rotational_max_period; // starting at minimum velocity
}

void rotational_trajectory_cb(void) {
  if ((rotational_total_steps / 2 <= rotational_reserve_steps) && (OCR5A > rotational_min_period)) { //if trajectory doesnt come to mid point or timer register doesnt reach its min
    rotational_increment_reg += 0.5 * rotational_accelaration; // increment register by 0.25 // 0.5 is beacause this function called half steps
    if (rotational_increment_reg == 2) {// if it become an int
      OCR5A -= 2; // increase velocity
      rotational_increment_reg = 0;
    }
  }
  else if ((rotational_total_steps / 2 >= rotational_reserve_steps) && rotational_steps_to_max_speed >= rotational_reserve_steps) {// if trajectory passes mid point and enough space to deaccelarate
    rotational_increment_reg += 0.5 * rotational_accelaration; // increment register by 0.25 // 0.5 is beacause this function called half steps
    if (rotational_increment_reg == 2) {// if it become an int
      OCR5A += 2;// decrease velocity
      rotational_increment_reg = 0;
    }
  }
  Serial.print("OCR5A =");
  Serial.println(OCR5A);
  Serial.print("Reserve Steps =");
  Serial.println(rotational_reserve_steps);
}

void rotational_calibrate() {
  if (rotational_finished) {
    rotational_current_pos = 0.00;
    float angle_to_correct = 0;
    uint8_t calibration_count = 0;
    uint16_t value, times = 0;
    Serial.println("Rotational Calibration Initiated.");

    do {
      digitalWrite(rotational_en_pin, LOW);
      do {
        uint16_t readings_array [100] = {} ;
        for (int i = 0 ; i < 100; i++) {
          readings_array[i] = round(magAlpha.readAngle());
          delay(5);
        }
        get_most_common(readings_array, 100, &value, &times);

        calibration_count ++;
        if (calibration_count > 5) {
          Serial.println("Rotational Calibration Most repeated value Error!!!!");
          break;
        }
      } while (times < 90);

      calibration_count = 0;
      Serial.print("Most Repeated Value: ");
      Serial.println(value);
      Serial.print("No of repetitions: ");
      Serial.println(times);
      float reading;
      uint16_t times_recorded = 0;
      float avg_angle = 0;
      for (int i = 0; i < 500; i++) { //taking 100 values to take average
        reading = magAlpha.readAngle();
        if (value == round(reading)) {
          avg_angle += reading;
          times_recorded++;
          //Serial.println(reading, 3);
        }
        delay(5);
      }
      Serial.print("Times Recorded: ");
      Serial.println(times_recorded);
      avg_angle /= times_recorded; //averaging
      angle_to_correct = -(rotational_zero - avg_angle);
      Serial.print("Average Angle = ");
      Serial.println(avg_angle, 3);
      Serial.print("Angle to correct = ");
      Serial.println(angle_to_correct);
      digitalWrite(rotational_en_pin, HIGH);
      rotational_runner(angle_to_correct); // calibrating
      calibration_count++;
      delay(1000);
      rotational_current_pos = 0.00;
      if (calibration_count > 5) {
        Serial.println("Rotational Calibration Error!!!!");
        break;
      }
    } while (abs(angle_to_correct) >= 0.3);

    rotational_current_pos = 0.00;
    EEPROM.put(rotational_ee_addr, rotational_current_pos);
    Serial.print("Rotational Current Position = ");
    Serial.println(rotational_current_pos);
    Serial.println("Rotational Calibration Finished");
    rotational_finished = true;
  }
}

void get_most_common(uint16_t arr[], uint16_t width, uint16_t *p_value, uint16_t *p_times) {
  uint16_t current_value, current_times = 0;
  *p_value = 0;
  *p_times = 0;
  for (int i = 0; i < width; i++) {
    current_value = arr[i];
    current_times = 0;
    for (int j = i; j < width; j++) {
      if (current_value == arr[j]) {
        current_times++;
      }
    }
    if (current_times > *p_times) {
      *p_value = current_value;
      *p_times = current_times;
    }
  }
}

