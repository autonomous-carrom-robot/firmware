void servo_init(void) {
  lifter.attach(lifter_pin);
  holder.attach(holder_pin);
  rotator.attach(rotator_pin);
  grabber.attach(grabber_pin);

  EEPROM.get(lifter_servo_ee_addr, lifter_servo_current_pos);
  EEPROM.get(holder_servo_ee_addr, holder_servo_current_pos);
  EEPROM.get(rotator_servo_ee_addr, rotator_servo_current_pos);

  grabber_servo_current_pos =  middle_disk;
  EEPROM.put(grabber_servo_ee_addr, grabber_servo_current_pos);

  lifter.write(lifter_servo_current_pos);
  holder.write(holder_servo_current_pos);
  rotator.write(rotator_servo_current_pos);
  grabber.write(grabber_servo_current_pos);
}

bool lifter_runner(uint8_t action) {
  if (lifter_higher <= action && action <= lifter_lower) { // bounding input
    if (action >= lifter_servo_current_pos) { // new position is greater than currrent position
      for (int16_t i = lifter_servo_current_pos; i <= action; i++) {
        lifter.write(i);
        delay(lifter_servo_delay);
      }
    }

    else { // new position is smaller than currrent position
      for (int16_t i = lifter_servo_current_pos; i >= action; i--) {
        lifter.write(i);
        delay(lifter_servo_delay);
      }
    }

    delay(500);
    lifter_servo_current_pos = action;
    EEPROM.put(lifter_servo_ee_addr, lifter_servo_current_pos);
  }
  else {
    Serial.println("");
    Serial.println("!!!!Invalid Input for Lifter Servo Destination");
    Serial.println("");
  }

  return true;
}

bool holder_runner(SERVO_ACTION action) {
  if (hold_shooter <= action && action <= release_shooter) { // bounding input
    if (action >= holder_servo_current_pos) { // new position is greater than currrent position
      for (int16_t i = holder_servo_current_pos; i <= action; i++) {
        holder.write(i);
        delay(holder_servo_delay);
      }
    }

    else { // new position is smaller than currrent position
      for (int16_t i = holder_servo_current_pos; i >= action; i--) {
        holder.write(i);
        delay(holder_servo_delay);
      }
    }

    delay(500);
    holder_servo_current_pos = action;
    EEPROM.put(holder_servo_ee_addr, holder_servo_current_pos);
  }
  else {
    Serial.println("");
    Serial.println("!!!!Invalid Input for Holder Servo Destination");
    Serial.println("");
  }
  return true;
}

bool rotator_runner(SERVO_ACTION action) {
  if (rotator_down <= action && action <= rotator_up) { // bounding input
    if (action >= rotator_servo_current_pos) { // new position is greater than currrent position
      for (int16_t i = rotator_servo_current_pos; i <= action; i++) {
        rotator.write(i);
        delay(rotator_servo_delay);
      }
    }

    else { // new position is smaller than currrent position
      for (int16_t i = rotator_servo_current_pos; i >= action; i--) {
        rotator.write(i);
        delay(rotator_servo_delay);
      }
    }

    delay(500);
    rotator_servo_current_pos = action;
    EEPROM.put(rotator_servo_ee_addr, rotator_servo_current_pos);
  }
  else {
    Serial.println("");
    Serial.println("!!!!Invalid Input for Rotator Servo Destination");
    Serial.println("");
  }
  return true;
}

bool grabber_runner(SERVO_ACTION action) {
  if (release_disk <= action && action <= grab_disk) { // bounding input
    if (action >= grabber_servo_current_pos) {// new position is greater than currrent position
      for (int16_t i = grabber_servo_current_pos; i <= action; i++) {
        grabber.write(i);
        delay(grabber_servo_delay);
      }
    }

    else { // new position is smaller than currrent position
      for (int16_t i = grabber_servo_current_pos; i >= action; i--) {
        grabber.write(i);
        delay(grabber_servo_delay);
      }
    }
    delay(500);
    grabber_servo_current_pos = action;
    EEPROM.put(grabber_servo_ee_addr, grabber_servo_current_pos);
  }
  else {
    Serial.println("");
    Serial.println("!!!!Invalid Input for Rotator Servo Destination");
    Serial.println("");
  }
  return true;
}

bool pendulum_pick_up(void) {
  holder.write(release_shooter);
  delay(1500);
  lifter.write(lifter_lower);
  delay(1000);
  holder.write(hold_shooter);
  delay(200);
  lifter.write(lifter_servo_current_pos);

  holder_servo_current_pos = hold_shooter;
  EEPROM.put(holder_servo_ee_addr, holder_servo_current_pos);

  //lifter_servo_current_pos = lifter_higher;
  //EEPROM.put(lifter_servo_ee_addr, lifter_servo_current_pos);
}

