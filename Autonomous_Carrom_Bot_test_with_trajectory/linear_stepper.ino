void linear_stepper_init(void) {
  pinMode (linear_en_pin, OUTPUT);
  pinMode (linear_dir_pin, OUTPUT);
  pinMode (linear_pulse_pin, OUTPUT);

  digitalWrite(linear_en_pin, HIGH);

  EEPROM.get(linear_ee_addr, linear_current_pos);
}

bool linear_runner (float destination) {
  if (linear_finished) { // no current linear motion
    if (linear_min_dis <= destination && destination <= linear_max_dis) { // bounding inputs
      OCR4A = 249;
      float distance_to_travel = destination - linear_current_pos;
      Serial.print("Linear Distance to Travel = ");
      Serial.println(distance_to_travel);

      digitalWrite(linear_dir_pin, (distance_to_travel > 0));
      Serial.print("Setting linear direction to = ");
      Serial.println(distance_to_travel > 0);

      linear_total_steps = round(abs(distance_to_travel) * 4.97795); // 180/(6.22 * 1.8 * pi) full steps
      linear_reserve_steps = linear_total_steps;
      //linear_steps = round(abs(distance_to_travel) * 10.235044); // 180/(6.22 * 0.9 * pi) half steps
      Serial.print("Setting linear steps to = ");
      Serial.println(linear_reserve_steps);

      if (linear_reserve_steps > 0) {
        //linear_trajectory_planning(); // calling the func to start timing
        en_linear_motion = true;
        linear_finished = false;
        linear_current_pos = destination;
        TIMSK4 |= (1 << OCIE4A);  //enable timer compare interrupt
      }
      else {
        TIMSK4 = 0x00;
        Serial.print("Linear Current Position =");
        Serial.println(linear_current_pos);
      }
    }
    else {
      Serial.println("");
      Serial.println("!!!!Invalid Input for Linear Destination");
      Serial.println("");
    }
  }
  return true;
}

void linear_callback(void) {
  if (linear_reserve_steps == 0) {
    TIMSK4 = 0x00;
    en_linear_motion = false;
    linear_finished = true;
    Serial.print("Linear Current Position =");
    Serial.println(linear_current_pos);
    digitalWrite(linear_pulse_pin, LOW);
    EEPROM.put(linear_ee_addr, linear_current_pos);
  }
  else {
    digitalWrite(linear_pulse_pin, !digitalRead(linear_pulse_pin));
    linear_reserve_steps -= digitalRead(linear_pulse_pin);
    //Serial.println(linear_reserve_steps);
  }
}

void linear_trajectory_planning(void) {
  linear_steps_to_max_speed = round((linear_max_period - linear_min_period) / linear_accelaration); //500 steps = 100.04mm
  //Serial.print("linear_steps_to_max_speed =");
  //Serial.println(linear_steps_to_max_speed);
  OCR4A = linear_max_period; // starting at minimum velocity
}

void linear_trajectory_cb(void) {
  if ((linear_total_steps / 2 <= linear_reserve_steps) && (OCR4A > linear_min_period)) { //if trajectory doesnt come to mid point or timer register doesnt reach its min
    linear_increment_reg += 0.5 * linear_accelaration; // increment register by 0.25 // 0.5 is beacause this function called half steps
    if (linear_increment_reg == 1) {// if it become an int
      OCR4A -= 1; // increase velocity
      linear_increment_reg = 0;
    }
  }
  else if ((linear_total_steps / 2 >= linear_reserve_steps) && linear_steps_to_max_speed >= linear_reserve_steps) {// if trajectory passes mid point and enough space to deaccelarate
    linear_increment_reg += 0.25;// increment register by 0.25 // 0.5 is beacause this function called half steps
    if (linear_increment_reg == 1) {// if it become an int
      OCR4A += 1;// decrease velocity
      linear_increment_reg = 0;
    }
  }
  /*Serial.print("OCR4A =");
    Serial.println(OCR4A);
    Serial.print("Reserve Steps =");
    Serial.println(linear_reserve_steps);*/
}

void linear_calibrate(void) {
  if (linear_finished) { // no current linear motion
    linear_finished = false; // disable other linear motions
    is_linear_calibrating = true; // enable calling callback function
    TIMSK4 |= (1 << OCIE4A);  //enable timer compare
    OCR4A = 249;

    digitalWrite(linear_dir_pin, LOW); // moving towards left
  }
}

void linear_calibrate_both(void) {
  if (linear_finished) { // no current linear motion
    linear_finished = false; // disable other linear motions
    is_linear_calibrating = true; // enable calling callback function
    TIMSK4 |= (1 << OCIE4A);  //enable timer compare
    OCR4A = 249;

    if (linear_destination <= 222.5) {
      digitalWrite(linear_dir_pin, LOW); // moving towards left
    }
    else {
      digitalWrite(linear_dir_pin, HIGH); // moving towards right
    }

  }
}

void linear_calibrate_callback (void) {
  linear_steps_to_calibrate += digitalRead(linear_pulse_pin); // counting steps needed to calibrate
  digitalWrite(linear_pulse_pin, !digitalRead(linear_pulse_pin));
}
